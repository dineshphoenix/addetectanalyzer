Contains python code to determine the best model classifier from 4 classifiers:

* Support Vector Machines
* k Nearest Neighbors
* Naive Bayes
* Decision Trees

Also contains the input .csv file upon which the classifier model is evaluated.

Install the packages used in the code ( to run on windows, linux by default has the packages when python is installed) 

To run:

python BestModelClassifier.py FeatureSelectedGTFixed.csv 