'''
arg 1 - input csv file (row = fv, last(nth) col = label, (n - 1)th col = apk name, (n-2)th col = part num, (n-3)th col = module name_

'''
import os
import sys
import numpy as np
import sklearn
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import SGDClassifier
from sklearn import metrics
from sklearn.pipeline import Pipeline
from sklearn import tree
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm
from sklearn.cross_validation import cross_val_score
from pprint import pprint
from random import shuffle
from sklearn.feature_selection import SelectKBest, chi2, f_classif
from sklearn.ensemble import ExtraTreesClassifier


class AdDetectAnalyzer:
    #########################################################################################################################################################
    #                                           Constructor                                                                                                 #
    #From a given set of samples, a training and test set is made and 4 classifiers (SVM, kNN, NB, DT) are evaluated and a best model classifier is selected#
    #########################################################################################################################################################
    
    def __init__(self,InputFile):
        self.Lines = [Line.strip() for Line in open (InputFile,'r').readlines()]
        self.Lines = self.Lines[1:]
        self.TotSamples = len (self.Lines)
        self.TrainSetSize = int(0.8 * float(self.TotSamples))
        self.TestSetSize = self.TotSamples - self.TrainSetSize
        #print self.TrainSetSize, self.TestSetSize

        self.TruncatedLines = self.GetTruncatedLines ()
        self.AllFVs = self.GetFVsFromTruncatedLines ()
        #print len(self.AllFVs[0])
        self.AllStrLabels, self.AllLabels = self.GetAllLabelsFromLines () 
        # print self.AllStrLabels
        # raw_input ("press any key")
        # print self.AllLabels

        self.TrainSamples = self.Lines[:self.TrainSetSize]#remove first line containing x1, x2,... = weka header
        self.TestSamples = self.Lines[self.TrainSetSize:]

        #print self.TrainSamples
        #print self.TestSamples
       
        self.TrainFVs = self.AllFVs[:self.TrainSetSize]
        self.TestFVs = self.AllFVs[self.TrainSetSize:]

        self.TrainLabels = self.AllLabels[:self.TrainSetSize]
        self.TestLabels = self.AllLabels[self.TrainSetSize:]

        self.Classifiers = [ 

        sklearn.grid_search.GridSearchCV(svm.LinearSVC(), 
        {'C':[0.001, 0.01, 0.1, 1, 10, 100, 1000], 
         'class_weight':[None,'auto'], 
         'loss':['l1', 'l2'] }, 
        cv=5, scoring = 'f1', n_jobs=10),

        sklearn.grid_search.GridSearchCV(KNeighborsClassifier(), 
        {'n_neighbors':[3,5,7,9,11,13,15]}, 
        cv=5, scoring = 'f1', n_jobs=10),

        MultinomialNB(),

        tree.DecisionTreeClassifier()
        
       ]

        self.BestModels = []


    ##################################################
    #                 Methods                        #
    ##################################################

    def GetTruncatedLines(self):
        TruncatedLines = []
        for Line in self.Lines:
            Comps = Line.split(",")[:-4]
            TruncatedLines.append(",".join(Comps))
        return TruncatedLines

    def GetFVsFromTruncatedLines(self):
        FVs = []
        for Line in self.TruncatedLines:
            FVs.append(np.fromstring(Line, dtype=int, sep=','))
        return FVs

    def GetAllLabelsFromLines(self):
        Labels = []
        for Line in  self.Lines:
            Labels.append(Line.split(",")[-1].strip())
        IntLabels = []
        for Label in Labels:
            if 'a' == Label.lower():
                IntLabels.append(1)
            elif 'a;o' == Label.lower():
                IntLabels.append(2)
            else:
                IntLabels.append(3)
        return Labels, IntLabels

    def SelectModel (self, ClassifierModels):
        BestModel = ClassifierModels.fit(self.TrainFVs, self.TrainLabels)

        # BestModelDecisionFunc = \
        # ClassifierModels.best_estimator_.decision_function
        return BestModel#, BestModelDecisionFunc
        #logger.info ("Model selection completed successfully through CV")

    def SelectModels(self):
        for ClassifierModels in self.Classifiers:
            #BestModel, BestModelDecisionFunc = self.SelectModel(ClassifierModels)
            BestModel = self.SelectModel(ClassifierModels)
            self.BestModels.append (BestModel)

    def EvaluateBestModel (self, Model, EvalChoice = 'test'):
        if (EvalChoice.lower() == 'test'):
            #logger.info ("Selected model is gonna be evaluated on test set")
            return Model.predict(self.TestFVs)
        elif (EvalChoice.lower() == 'train'):
            #logger.info ("Selected model is gonna be evaluated on \
            #training set itself")
            return Model.predict(self.TrainFVs)
        else:
            #logger.error ("invalid choice of dataset for evaluating\
            #the chosen model. Pass either 'train' or 'test' as arg")
            return -1

    def EvaluateTestSetOnClassifiers(self):
        for Model in self.BestModels:
            print "*******"*5
            print "evlauting ", Model
            PredictedLabels = self.EvaluateBestModel(Model,'test')
            Accuracy = np.mean(PredictedLabels == self.TestLabels)
            print "Test Set Accuracy = ", Accuracy
            print(metrics.classification_report(self.TestLabels, 
                PredictedLabels, target_names=['ad', 'both', 'other']))
            print "*******"*5


def main():

    InputFile = sys.argv[1]

    AdDetectAnalyzerObj = AdDetectAnalyzer(InputFile)

    #currently 4 classifiers are considered: kNN, NB, DT, SVM
    AdDetectAnalyzerObj.SelectModels() 

    AdDetectAnalyzerObj.EvaluateTestSetOnClassifiers()



if __name__ == '__main__':
    main()







        

    

        





























